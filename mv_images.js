const argv = require('minimist')(process.argv.slice(2), {
  string: [ 'flat' ],
  alias: { f: 'flat' },
  default: { flat: false },
});

const fs = require('fs');
const path = require('path');
const isFlat = typeof argv.flat == 'string' || argv.flat === true;
const subdir = isFlat ? 'entries' : 'entries/Orlando-images';
const dirname = path.join(__dirname, subdir);

if (!fs.lstatSync(dirname).isDirectory()) {
  console.log(`${dirname} is not a valid folder!`);
  return;
}

const xml2js = require('xml2js');
const parser = new xml2js.Parser({ attrkey: "ATTR" });

const bags = fs.readdirSync(dirname);

function getModsFile(data_dir) {
  const files = fs.readdirSync(data_dir).filter(file => {
    return path.extname(file).toLowerCase() === '.xml';
  });
  return files ? files[0] : '';
}

function getEntryId(mods) {
  const xml_string = fs.readFileSync(mods, 'utf8');
  let entry_id = '';

  parser.parseString(xml_string, (err, res) => {
    if (err === null && res.mods.hasOwnProperty('subject')) {
      res.mods.subject.forEach(subject => {
        if (entry_id || !subject.hasOwnProperty('name')) { return; }

        entry_id = subject.name[0].namePart[0];
      });
    }
    else {
      console.warn(`This "${mods}" MODS file doesn't have the subject name at the expected place.`);
    }
  });
  return entry_id;
}

function createEntryDir(entry_id) {
  if (!entry_id || entry_id === '' || entry_id.length > 6) {
    return;
  }

  const entry_dir = path.join(dirname, entry_id);
  if (!fs.existsSync(entry_dir)) {
    fs.mkdirSync(entry_dir);
  }
  return true;
}

function moveBagToEntryDir(bag, entry_id) {
  const entry_dir = path.join(dirname, entry_id);
  const bag_folder = path.join(dirname, bag);
  fs.rename(bag_folder, `${entry_dir}/${bag}`, (err) => {
    if (err) throw err;
    console.log(`${bag_folder} have been successfully moved at ${entry_dir}/${bag}`);
  });
}

bags.forEach(bag => {
  if (isFlat && !bag.endsWith('.xml')) { return; }

  if (bag.startsWith(!isFlat ? 'Bag-orlando' : 'orlando_')) {
    const data_dir = isFlat ? dirname : path.join(dirname, `${bag}/data`);
    let mods = isFlat ? bag : getModsFile(data_dir);
    console.log(`Currently processing ${mods}`);

    if (!mods) {
      console.warn(`Couldn't find the proper mod file to work with!`);
      return;
    }

    const entry_id = getEntryId(path.join(data_dir, `/${mods}`));
    if (entry_id && createEntryDir(entry_id)) {
      if (isFlat) {
        bags.forEach(file => {
          if (!file.startsWith(mods.replace('.xml', ''))) { return; }
          moveBagToEntryDir(file, entry_id);
        });
      }
      else {
        moveBagToEntryDir(bag, entry_id);
      }
    }
    else {
      console.warn(`We couldn't extract the entry id from the mods for ${bag}`);
    }
  }
});
