# XML entity files rename

## 🛠 Requirements

- [nodejs](https://nodejs.org/)
- [yarn](https://yarnpkg.com/getting-started/install#global-install): Sometimes it get installed with yarn

## 🚀 Getting started

1. Clone this folder on your computer.
2. Run `yarn install` to install the project dependencies.
3. Create a folder called `entries`.
4. Copy or move all the bagit directories inside the `entries` folder.

## 🔨 Usage

### Renaming entries

To rename the entries you will need to provide the following arguments:

- `dir_prefix`: (Optional) the bag folder prefix which is set to `Bag-` by default.
- `file_prefix`: The prefix of the entity file to rename. For example for organization it could be `organization-pubc-`.
- `file_ext`: The extension of the file to be saved. By default it's `xml`. For example for image it could be `jpg`.

Then the commands could look like:

```bash
node bulk_rename.js --file_prefix organization-pubc-
```

In case the directory prefix is needed then it could look like

```bash
node bulk_rename.js --dir_prefix Bag- --file_prefix organization-pubc-
```

Also note that `-d`, `-e` and `-f` are aliases for `dir_prefix`, `file_ext` and `file_prefix` respectively.

```bash
node bulk_rename.js -d Bag- -f organization-pubc-
```

or

```bash
node bulk_rename.js -d Bag- -f image-pubc- -e jpg
```

### Moving images

To move images you need to provide the following arguments:

- `flat`: (optional) to specify that the directory is flat and doesn't follow the bagit structure.

Then command could look like

```bash
node mv_images.js -f true
```

or use the command below if the folder has the bagit structure.

```bash
node mv_images.js
```
