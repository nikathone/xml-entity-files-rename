
const argv = require('minimist')(process.argv.slice(2), {
  string: [ 'dir_prefix', 'file_prefix', 'file_ext' ],
  alias: { d: 'dir_prefix', f: 'file_prefix', e: 'file_ext' },
  default: { dir_prefix: 'Bag-', file_prefix: 'organization-pubc-', file_ext: 'xml' },
});

const xml2js = require('xml2js');
const parser = new xml2js.Parser({ attrkey: "ATTR" });

const fs = require('fs');
const path = require('path');
const directoryPath = path.join(__dirname, 'entries');

const dir_prefix = argv.d;
const file_prefix = argv.f;
const file_ext = argv.e;

function xmlEntityRename(dirname) {
  if (!dirname.startsWith(dir_prefix)) {
    return console.log(`Skipped ${dirname}`);
  }

  const data_dir = `${directoryPath}/${dirname}/data`;
  const pid = getEntityPid(data_dir).replace(':', '_');
  const new_file = `${pid}.${file_ext}`;
  const old_file = getFile2Rename(data_dir);

  if (pid && old_file) {
    fs.rename(`${data_dir}/${old_file}`, `${data_dir}/${new_file}`, (err) => {
      if (err) throw err;
      console.log(`${old_file} have been successfully renamed to ${new_file}`);
    });
  }
  else if (!old_file) {
    console.warn(`No file starting with "${file_prefix}" was found in "${data_dir}"`);
  }
}

function getEntityPid(dirname) {
  const dc = `${dirname}/metadata/dublincore.xml`;
  const xml_string = fs.readFileSync(dc, 'utf8');
  let pid;

  parser.parseString(xml_string, (err, res) => {
    pid = err === null ? res['oai_dc:dc'] : null;
  });
  return pid['dc:identifier'][0];
}

function getFile2Rename(dirname) {
  const files = fs.readdirSync(dirname);
  let file2rename = null;

  files.forEach(file => {
    const filename = path.join(dirname, file);
    const stat = fs.lstatSync(filename);
    if (!stat.isDirectory() && !(filename.indexOf('.Identifier') >= 0) && (filename.indexOf(`${file_prefix}`) >= 0)) {
      file2rename = file;
    }
  });
  return file2rename;
}

fs.readdir(directoryPath, function (err, files) {
  if (err) {
    return console.log('Unable to scan directory: ' + err);
  }

  files.forEach(file => {
    xmlEntityRename(file);
  });
});
